describe('Tickets', () => {
    
    // Execute before execute the scenarios(it)
    beforeEach(() => cy.visit('https://ticket-box.s3.eu-central-1.amazonaws.com/index.html'));

    // you can use mocha features in cypress, example it.only to run just one scenario
    it('Fill all text fields', () => {
        let firstName = 'testeFirstName'
        let lastName = 'testeLastName'

        cy.get("input[id='first-name']").type(firstName);
        cy.get("input[id='last-name']").type(lastName);
        cy.get("input[id='email']").type('testeLastName@mailinator.com');
        cy.get("textarea[id='requests']").type('TESTE TESTE TESTE');
        cy.get("input[id='signature']").type(`${firstName} ${lastName}`);
    });

    it('Select all select fields', () => {
        cy.get("select[id='ticket-quantity']")
          .select('2');
    });

    it('checking a radiobutton', () => {
        cy.get("input[id='vip']").check();
    });

    it('checking and unchecking checkbox', () => {
        cy.get("input[id='friend']")
          .check();
        cy.get("input[id='publication']")
          .check();
        cy.get("input[id='social-media']")
          .check();
        cy.get("input[id='friend']")
          .uncheck();

        cy.get("input[id='agree']")
          .check();
    });

    it('has text TTICKETBOX', () => {
        // this a example of a assertion (should(option, text))
        cy.get('header h1')
          .should('contain', 'TICKETBOX');
    });

    it('Checking the behavior of invalid email', () => {
        // in cypress you can concact methods
        // is more readable if you jump a line when you do that

        cy.get("#email")
          .as('email') // this is a alias as ('email')
          .type('testeLastNamemailinator.com');
        
        cy.get("#email.invalid")
          .as('invalid email') // this is a alias as ('invalid email')
          .should('exist');

        cy.get('@email') // calling the alias above in this scenarios .as('email')
          .clear()
          .type('testeLastName@mailinator.com');

        cy.get('invalid email') // calling the alias above in this scenarios ('invalid email')
          .should('not.exist');
    });

    it('Putting all togheter and reset all fields', () => {
        let firstName = 'testeFirstName'
        let lastName  = 'testeLastName'
        let fullName  = `${firstName} ${lastName}`
        let qtyNumber = 2

        cy.get("input[id='first-name']").type(firstName);
        cy.get("input[id='last-name']").type(lastName);
        cy.get("input[id='email']").type('testeLastName@mailinator.com');
        cy.get("textarea[id='requests']").type('TESTE TESTE TESTE');
        cy.get("input[id='signature']").type(`${fullName}`);
        cy.get("select[id='ticket-quantity']").select(`${qtyNumber}`);
        cy.get("input[id='vip']").check();
        cy.get("input[id='friend']").check();

        cy.get(".agreement p").should("contain", `I, ${fullName}, wish to buy ${qtyNumber} VIP`);
        cy.get("input[id='agree']").click(); // you can click here

        cy.get("button[type='submit']")
          .as('submitButton')
          .should('not.be.disabled');
        
        cy.get("button[type='reset']").click();
        
        cy.get('@submitButton').should('be.disabled');
    });

    it('fills mandatory fields using support > commands.js', () => {
        let customer = {
            firstName: 'testeFirstName',
            lastName: 'testeLastName',
            email: 'testeLastName@mailinator.com'
        };

        cy.fillMandatoryFields(customer)

        cy.get("button[type='reset']").click();
        cy.get("button[type='submit']").should('be.disabled');
    });
});

// all test needs
// Precoditions (example: visit and others checks)
// Actions
// Assertion (should check something)

// cypress do not recommend using Page Object
// https://www.cypress.io/blog/2019/01/03/stop-using-page-objects-and-start-using-app-actions/
// They recommend using customized actions cypress > support > commands.js